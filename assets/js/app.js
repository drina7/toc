$('body').tableOfContents({
    ulClass: 'list-group',
    appendAfterSelector: '#data'
});

$( document ).ready(function() {
    const olList = [1, 2, 3, 5, 7, 8];//<ol> tag position
    const ulList = [4, 6];//<ul> tag position
    olElement(olList);
    ulElement(ulList);
    $('#data').empty();
    $('#data').remove();

});


function element(data_id){
    return $('ol[data-id='+data_id+']');
}

//Check class value for lists
function checkClass(data_id){
    switch(data_id){
        case 1:
            return 'decimal';
        break;
        case 2:
            return 'initial';
        break;
        case 3:
            return 'upper-roman';
        break;
        case 4:
            return 'circle';
        break;
        case 5:
            return 'lower-roman';
        break;
        case 6:
            return 'square';
        break;
        case 7:
            return 'upper-alpha';
        break;
        case 8:
            return 'lower-alpha';
        break;
        default:
            return '';
        break;
    }
}

//Add list-style-type for order lists
function olElement(el){
    for(let i=0;i<el.length;i++){
        let val = el[i];
        element(val).addClass(checkClass(val));
    }
}

//Add list-style-type for unorder lists
function ulElement(el){
    let temp = 0;
    for(let i=0;i<el.length;i++){
        let val = el[i];
        element(val).replaceWith(function(){
        return $("<ul />", {html: $(this).html()});
    });
    if(temp == 0){
        $('ul').addClass(checkClass(val));
    } else {
        $('ul').not('.'+checkClass(temp)).addClass(checkClass(val));
    }
    temp = val;
    }
}   
