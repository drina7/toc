(function($) {

	// Default settings.
	var defaults = {
		ulClass: null,
		appendAfterSelector: 'h1'
	};
	
	var opt = null;
	
	jQuery.fn.tableOfContents = function(options) {
		
	
		opt = $.extend(true, defaults, options);
		
		var MIN = 1;
		var MAX = 8;
		
		var li = MIN;
		
		var counter = 1;

		var out = '<ol';
		if (opt.ulClass) {
			out += ' class="' + opt.ulClass + '" data-id="1"';
		}
		out += '>';


		
		function buildListItems(headers, headerNum) {

			if (headerNum > MAX) {
				return '';
			}
		
			$(headers).each(function() {
				
				var self = $(this);
				var selectorel = 'li.' + headerNum;
				var nselectorel = 'li.' + (headerNum+1);
				var hnext = self.nextUntil(selectorel, nselectorel);		
				var hnextLength = hnext.size();
				
				out += '<li>';
				out += '<a title="'+ $.trim(self.text()) +'" href="contents/'+counter+'.html" target="_blank">';			
				out +=  self.text();
				out += '</a>';
				counter++;
				
				if (hnextLength) {
					let i = headerNum + 1;
					out += "<ol data-id='"+i+"'>";
					buildListItems(hnext, headerNum + 1);
					out += '</ol>';
				}
				out += '</li>';
			});

		}
		
		
		buildListItems(this.find('li.' + li), li);
		
		out += '</ol>';
		
		var appendAfter = $(opt.appendAfterSelector);

		
		if (appendAfter) {
		
			appendAfter.after($(out));
		
		}
		
		return this;

		
		
	};
	
}) (jQuery);


