<?php
header('Content-type: text/html; charset=utf-8');

$filepath = "inc/input.txt";  //Input File Path

//Add <pre></pre> tags to input file
function file_append ($filepath) {
    $fileContent = file_get_contents ($filepath);
    if(strpos($fileContent, "<pre>") === false){
        file_put_contents ($filepath, "<pre>\n" . $fileContent);
    } else if(strpos($fileContent, "</pre>") === false){
        file_put_contents($filepath, "\n</pre>" , FILE_APPEND | LOCK_EX);
    }   
}

file_append($filepath);
$lines = file($filepath);
$last = count($lines)-1;
$html[] = "<ol class='list-group decimal'>";
foreach ($lines as $line_num => $line) {
    if($line_num == 0 || $line_num == $last){
        continue;
    }
    $sum = $sum + substr_count($line, ' ');
    $tab = 0;
    if (preg_match("/\t/", $line)) {
        $tab = strspn($line, "\t");
    }
    $i = $tab + 1;
    if(strlen($line) > 1){
        $html[] = "<li class='".$i."'>" . $line . "</li>";
    }

}
$html[] = "</ol>";



?>
