<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Table Of Contents</title>
        <script type="text/javascript" src="assets/js/jquery-1.12.4.min.js"></script>
        <script type="text/javascript" src="assets/js/jquery.tableOfContents.js"></script>
        <link rel="icon" href="assets/img/book.ico" sizes="16x16"> 
        <link rel="stylesheet" href="assets/css/app.css">
    </head>
    <body>
    <div class="container" id="container">
        <h1>Table of Contents</h1>
        <div id="data">
            <?php 
                include 'inc/toc.php';
                for($i=0;$i<count($html);$i++){
                    echo $html[$i];
                }
            ?>
         </div>
        <script type="text/javascript" src="assets/js/app.js"></script>
        </div>
        </body>
</html>